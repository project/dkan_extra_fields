## INTRODUCTION

The DKAN extra fields module provides JSON Schema property values in extra fields
in Drupal field display UI.

## REQUIREMENTS

[DKAN](https://github.com/GetDKAN/dkan)
and apply patch 4310-plus.patch that is given with this module.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.
DKAN module needs to be enabled (specifically metastore_search module).
This needs to be done manually due to this [issue](https://github.com/GetDKAN/dkan/discussions/4149)

## CONFIGURATION
Put the desired extra fields in the output of your node view
admin/structure/types/manage/data/display for example

## CUSTOMIZATION

### dkan_extra_field_item - Outputs Items that have multiple properties in one extra field
You can use hook_preprocess_HOOK() for dkan_extra_field_item to control
the output of labels and values for the extra field items. Check the output
of $variables in the hook and see that there is an array keys that contains
the property key names. You can hide key and value or change the key label.
Also dkan_extra_field_item has a lot of theme_suggestions to vary on.

## MAINTAINERS

Current maintainers:

- Stefan Korn - https://www.drupal.org/u/stefankorn

